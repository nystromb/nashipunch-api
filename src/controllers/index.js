const postController = require("./postController");
const threadController = require("./threadController");
const authController = require("./authController");
const userController = require("./userController");
const subforumController = require("./subforumController");

module.exports = {
  postController,
  threadController,
  authController,
  subforumController,
  userController,
};