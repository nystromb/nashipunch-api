const roundNumber = num => Math.ceil(num);

module.exports = {
  roundNumber,
};