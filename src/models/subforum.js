'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subforum = sequelize.define('Subforum', {
    name: DataTypes.STRING,
    icon_id: DataTypes.INTEGER,
    createdAt: {type: DataTypes.DATE, field: 'created_at'},
    updatedAt: {type: DataTypes.DATE, field: 'updated_at'},
  }, {
    timestamps: true
  });
  Subforum.associate = function(models) {
    Subforum.Threads = Subforum.hasMany(models.Thread, { as: 'threads', foreignKey: 'subforum_id' });
  };
  Subforum.initScopes = () => {
    Subforum.addScope('withThreads', () => ({
      include: [
        {
          association: Subforum.Threads,
          attributes: ['id', 'title', 'icon_id', 'user_id' , 'created_at', 'updated_at'],
        },
      ],
    }));
  }
  return Subforum;
};